import React from 'react';
import ReactDOM from 'react-dom';
import MarkdownEditor from './editor/editor';

const editorContainer = document.getElementById('react-editor');
if (editorContainer) {
  const props = JSON.parse(editorContainer.dataset.reactProps) || {};
  const tokenTag = document.querySelector('meta[name="user-token"]');
  if (tokenTag) props.userToken = tokenTag.content;
  ReactDOM.render(
    React.createElement(MarkdownEditor, props), editorContainer
  );
}
