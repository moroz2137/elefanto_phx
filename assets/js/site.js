// import 'materialize-css';
import "phoenix_html";
import Prism from 'prismjs';
import _ from 'lodash';
import React from 'react';
import ReactDOM from 'react-dom';
import Comments from './comments/Comments';

Prism.highlightAll();

const setDocumentHeight = () => {
  const body = document.body,
    html = document.documentElement;

  window.documentHeight = Math.max(
    body.scrollHeight, body.offsetHeight,
    html.clientHeight, html.scrollHeight, html.offsetHeight
  );
}

const getInitialBackgroundPositionY = (el) => {
  const str = window.getComputedStyle(el).getPropertyValue('background-position-y');
  return parseFloat(str);
}

const setParallaxEffect = () => {
  setDocumentHeight();
  const parallaxDiv = document.getElementById('parallax');
  const startPosition = getInitialBackgroundPositionY(parallaxDiv);
  document.querySelectorAll('.post-content img').forEach(img => {
    img.addEventListener('load', setDocumentHeight);
  });
  window.addEventListener('resize', setDocumentHeight);
  window.addEventListener('scroll', (e) => {
    // how many percentiles down the background should scroll
    const positionDiff = 25;

    // values of window.scrollY when parallax is triggered
    // e.g. it starts scrolling 150px down the page
    // and stops 150px from the bottom of the document
    const startScrollY = 30;
    const endScrollY = window.documentHeight - startScrollY;

    // y === scrolling offset relative to startScrollY
    const maxY = endScrollY - startScrollY;
    const y = window.scrollY - startScrollY;

    if (y < 0 || y > maxY) return;

    let newPosition = _.round(((y / maxY * positionDiff) + startPosition), 2);
    parallaxDiv.style.backgroundPositionY = `${newPosition}%`;
  });
}

function initCommentsApp(container) {
  if (!container) return;
  const props = JSON.parse(container.dataset.commentsJson);
  props.recaptchaKey = container.dataset.recaptchaKey;
  props.postId = container.dataset.postId;
  ReactDOM.render(
    React.createElement(Comments, props), container
  );
}

document.addEventListener('DOMContentLoaded', () => {
  // M.AutoInit();
  if (window.innerWidth >= 500) setParallaxEffect();
  // don't bother setting up parallax effect on mobile devices
  const commentsContainer = document.getElementById('commentsApp');
  initCommentsApp(commentsContainer);

  if (document.forms.new_comment_form) {
    const form = document.forms.new_comment_form;
    const button = document.getElementById('new_comment_submit');
    form.addEventListener('submit', handleCommentSubmit);
    document.getElementById('comment_body').addEventListener('focus', () => {
      if (window.commentFormExpanded) return;
      window.commentFormExpanded = true;
      form.classList.add('is-expanded');
      window.grecaptcha && grecaptcha.render('new_comment_recaptcha', {
        sitekey: '6Lf7Tm0UAAAAAJBAsWnsc7jivqVoRaIYj0Om2sNR',
        callback: () => {
          button.disabled = false;
        }
      })
    })
  }
});
