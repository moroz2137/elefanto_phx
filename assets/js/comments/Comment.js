import React from 'react';
import moment from 'moment';

const Comment = ({ comment }) => {
  const { body, signature, inserted_at } = comment;
  const timestamp = moment(inserted_at);
  return (
    <div className="comment">
      <p className="comment-metadata">
        <span className="comment-signature">{signature}</span>
        <span className="comment-timestamp">{timestamp.format('D MMM., YYYY [at] HH:mm Z')}</span>
      </p>
      <p className="comment-body">
        {body}
      </p>
    </div>
  )
}

export default Comment;