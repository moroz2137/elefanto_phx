import React, { Fragment } from 'react';
import CommentForm from './CommentForm';
import Comment from './Comment';

const CommentCount = ({ count }) => {
  if (!count) return <h4>No comments</h4>;
  if (count === 1) return <h4>One comment</h4>;
  return <h4>{count} comments</h4>
}

class Comments extends React.Component {
  state = {
    comments: [],
    body: '',
    bodyError: null,
    signature: '',
    signatureError: null,
    expanded: false,
    submitting: false,
    recaptchaToken: null,
    verified: false
  }

  submitComment = (e) => {
    e.preventDefault();
    const { body, signature, recaptchaToken } = this.state;
    const { postId } = this.props;
    const data = JSON.stringify({
      comment: {
        body, signature
      },
      'g-recaptcha-response': recaptchaToken, post_id: postId
    });
    this.setState({ submitting: true })
    fetch('/api/comments', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: data
    })
      .then(data => data.json())
      .then(data => {
        if (data.errors) {
          this.parseErrors(data.errors);
        } else {
          this.addComment(data);
          this.resetForm();
        }
      })
      .finally(() => {
        this.captcha && this.captcha.reset();
        this.setState({ submitting: false })
      });
  }

  captchaRef = (e) => {
    this.captcha = e;
  }

  parseErrors = (errors) => {
    // if there are no errors for these fields, reset
    // their error state
    const newState = {
      bodyError: null,
      signatureError: null
    };
    errors.forEach(e => {
      const path = e.source.pointer;
      const attrName = path.split('/').slice(-1)[0];
      newState[`${attrName}Error`] = e.title;
    });
    this.setState(newState);
  }

  resetForm = () => {
    this.setState({
      expanded: false,
      verified: false,
      body: '',
      signature: '',
      bodyError: null,
      signatureError: null
    });
  }

  componentDidMount() {
    const { recaptchaKey, comments } = this.props;
    this.setState({ comments, verified: !recaptchaKey });
  }

  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value })
  }

  recaptchaCallback = (response) => {
    this.setState({ verified: true, recaptchaToken: response });
  }

  expiredCallback = () => {
    this.setState({ verified: false });
  }

  onFocus = () => {
    this.setState({ expanded: true });
  }

  addComment = (data) => {
    const newComment = data.data.attributes;
    const timestamp = newComment['inserted-at'];
    newComment.inserted_at = timestamp;
    delete newComment['inserted-at'];
    const comments = this.state.comments;
    comments.unshift(newComment);
    this.setState({ comments });
  }

  render() {
    const { comments, recaptchaKey } = this.props;
    const { body, signature, bodyError, signatureError, expanded, verified, submitting } = this.state;
    return (
      <Fragment>
        <CommentForm
          recaptchaKey={recaptchaKey}
          recaptchaCallback={this.recaptchaCallback}
          onChange={this.onChange}
          onFocus={this.onFocus}
          body={body}
          signature={signature}
          bodyError={bodyError}
          signatureError={signatureError}
          captcha={this.captcha}
          expanded={expanded}
          verified={verified}
          submitComment={this.submitComment}
          submitting={submitting}
          expiredCallback={this.expiredCallback}
        />
        <CommentCount count={comments.length} />
        {comments.map(comment => (
          <Comment comment={comment} key={`comment-${comment.id}`} />
        ))}
      </Fragment>
    )
  }
}

export default Comments;