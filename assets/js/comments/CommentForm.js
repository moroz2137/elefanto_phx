import React, { Fragment } from 'react';
import Reaptcha from 'reaptcha';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { TextField, Button } from '@material-ui/core';
import { blue, teal } from '@material-ui/core/colors';

const theme = createMuiTheme({
  palette: {
    primary: teal,
    secondary: blue,
  },
});

class CommentForm extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.captchaSize = window.innerWidth > 600 ? 'normal' : 'compact';
    this.props.captcha && this.props.captcha.renderExplicitly();
  }

  render() {
    const {
      recaptchaKey, recaptchaCallback, verified, onChange, body,
      signature, submitting, submitComment, captcha, captchaRef,
      expanded, onFocus, bodyError, signatureError
    } = this.props;

    const canSubmit = verified && body && signature && !submitting;

    return (
      <MuiThemeProvider theme={theme}>
        <form className="comment-form" onSubmit={submitComment}>
          <i className="material-icons">comment</i>

          <TextField
            id="comment_body"
            name="body"
            label="Leave a comment..."
            multiline
            rowsMax={5}
            onChange={onChange}
            onFocus={onFocus}
            error={!!bodyError}
            helperText={bodyError || 'Min. 10 characters. All HTML tags will be removed.'}
            value={body}
            style={{
              gridColumn: '2/4'
            }}
          />

          {expanded ?
            <Fragment>
              <TextField
                id="comment_signature"
                name="signature"
                label="Signature"
                error={!!signatureError}
                helperText={signatureError}
                value={signature}
                onChange={onChange}
              />

              {recaptchaKey ? <Reaptcha
                ref={captchaRef}
                sitekey={recaptchaKey}
                onVerify={recaptchaCallback}
                size={this.captchaSize}
                render="explicit"
              /> : ''}

              <Button
                variant="contained"
                color="primary"
                className="button waves-effect waves-light comment-form-submit"
                onClick={submitComment}
                disabled={!canSubmit}
              >
                Submit your comment
          </Button>
            </Fragment>
            : ''}
        </form>

      </MuiThemeProvider>
    )
  }
}

export default CommentForm;