import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { TextField } from '@material-ui/core';

const genericClasses = "h2-size";

const NumberTitleInputWrapper = styled.section`
  margin-bottom: 1rem;
  margin-top: 1rem;
  display: grid;
  grid-template-columns: 3rem 1fr;
  grid-column-gap: 1rem;
  grid-row-gap: 10px;
`;

class NumberTitleInput extends React.Component {
  static propTypes = {
    handleChange: PropTypes.func,
    handleSlugify: PropTypes.func,
    number: PropTypes.string,
    title: PropTypes.string,
    slug: PropTypes.string
  };

  state = {
    slugModified: false
  }

  handleBlur = () => {
    if (this.state.slugModified) return;
    this.props.handleSlugify();
  }

  handleSlugChange = (e) => {
    this.setState({ slugModified: true });
    this.props.handleChange(e);
  }

  render() {
    const {
      number, title, slug, errorList, handleChange
    } = this.props;

    const { numberError, titleError, slugError } = errorList;
    return (
      <NumberTitleInputWrapper>
        <TextField
          value={number}
          type="text"
          name="number"
          label="#"
          onChange={handleChange}
          onBlur={this.handleBlur}
          error={!!numberError}
        />

        <TextField
          value={title}
          className={genericClasses}
          type="text"
          name="title"
          multiline
          label="Title"
          placeholder="Another boring title"
          onChange={handleChange}
          onBlur={this.handleBlur}
          error={!!titleError}
        />

        <TextField
          value={slug}
          type="text"
          name="slug"
          label="Slug"
          helperText="This will be used for URL generation."
          onChange={this.handleSlugChange}
          error={!!slugError}
          style={{ gridColumn: '1/3' }}
        />
      </NumberTitleInputWrapper>
    );
  }
}

export default NumberTitleInput;