import axios from 'axios';
import { EditorState, ContentState } from 'draft-js';

class ApiClient {
  constructor({ userToken }) {
    this.instance = axios.create({
      baseURL: '/api',
      headers: {
        'Content-Type': 'application/vnd.api+json',
        'Authorization': `Bearer ${userToken}`
      }
    })
  }

  async getSlug({ number, title }) {
    try {
      const response = await this.instance.get(
        '/slugify_title',
        {
          params: { number, title }
        }
      );
      const { slug } = response.data.data;
      return { slug };
    } catch (e) { }
  }

  async savePost(state) {
    const { id } = state;
    const data = this.serializePost(state);
    const url = this.postPath(id);
    let response, newId;
    try {
      if (id) {
        response = await this.instance.patch(url, data);
      } else {
        response = await this.instance.post(url, data);
        newId = parseInt(response.data.data.id);
      }
      const newState = {
        errors: {},
        messageOpen: true,
        message: {
          text: 'The post has been saved.',
          variant: 'success'
        }
      };
      if (newId) newState.id = newId;
      return newState;
    } catch (err) {
      return this.parseValidationErrors(err);
    }
  }

  async loadPost(id) {
    if (id) {
      const url = `/posts/${id}`;
      const response = await this.instance.get(url);
      return this.responseToState(response);
    } else {
      return { loading: false };
    }
  }

  parseValidationErrors(error) {
    const errors = error.response.data.errors;
    let errorState = {};
    errors.forEach(e => {
      const path = e.source.pointer;
      const attrName = path.split('/').slice(-1)[0];
      errorState[`${attrName}Error`] = e.detail;
    });
    const message = _.join(Object.values(errorState), ", ");
    return { errors: errorState, messageOpen: true, message: { text: message, variant: 'error' } };
  }

  postPath(id) {
    return id ? `/posts/${id}` : '/posts';
  }

  serializePost(state) {
    const { title, number, slug, language, editorState, renderingEngine } = state;
    const body = editorState.getCurrentContent().getPlainText();
    return {
      post: { body, title, number, language, slug, rendering_engine: renderingEngine }
    };
  }

  responseToState(response) {
    const data = response.data.data;
    const { language, number, title, slug, body } = data.attributes;
    const renderingEngine = data.attributes['rendering-engine'];
    const id = data.id;
    const editorState = EditorState.createWithContent(
      ContentState.createFromText(body)
    );
    return {
      loading: false,
      language, number, title, slug, id, editorState, renderingEngine
    };
  }

}

export default ApiClient;