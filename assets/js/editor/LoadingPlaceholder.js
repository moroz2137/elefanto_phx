import React from 'react';
import styled from 'styled-components';

const PlaceholderContainer = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const PlaceholderText = styled.h2`
  margin: auto;
`;

const LoadingPlaceholder = () => (
  <PlaceholderContainer>
    <PlaceholderText>Loading data...</PlaceholderText>
  </PlaceholderContainer>
)

export default LoadingPlaceholder;