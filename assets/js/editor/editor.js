import React from 'react';
import PropTypes from 'prop-types';
import { Editor, EditorState } from 'draft-js';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { teal, indigo } from '@material-ui/core/colors';
import styled from 'styled-components';

import Appbar from './Appbar';
import NumberTitleInput from './NumberTitleInput';
import StatusBar from './StatusBar';
import LoadingPlaceholder from './LoadingPlaceholder';

import ApiClient from './ApiClient';

const theme = createMuiTheme({
  palette: {
    primary: indigo,
    secondary: teal,
  },
});

const EditorOuterWrapper = styled.div`
  overflow-y: auto;
  width: 100vw;
  padding: 0 1rem;
`;

const EditorWrapper = styled.main`
  max-width: 50em;
  margin: 0 auto;
`;

class MarkdownEditor extends React.Component {
  static propTypes = {
    onChange: PropTypes.func
  };

  state = {
    editorState: EditorState.createEmpty(),
    errors: {},
    loading: true,
    messageOpen: false,
    message: {
      text: '',
      variant: ''
    },
    language: 'en',
    renderingEngine: 'markdown',
    number: '',
    title: '',
    slug: '',
  }

  constructor(props) {
    super(props);
    this.apiClient = new ApiClient({ userToken: this.props.userToken });
  }

  async componentDidMount() {
    const newState = await this.apiClient.loadPost(this.props.id);
    this.setState(newState);
  }

  goBack = () => {
    if (this.state.id) {
      var url = `/blog/${this.state.slug}`;
    } else {
      var url = '/blog';
    }
    window.location.href = url;
  }

  handleSlugify = async () => {
    const { number, title } = this.state;
    if (!(number && title)) return;

    const slug = await this.apiClient.getSlug({
      number, title
    });
    slug && this.setState(slug);
  }

  onChange = (editorState) => {
    this.setState({ editorState });
  }

  handleCloseSnackbar = () => {
    this.setState({ messageOpen: false });
  }

  handleSave = async (e) => {
    const newState = await this.apiClient.savePost(this.state);
    this.setState(newState);
  }

  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  render = () => {
    const {
      loading, number, slug, title, errors, language, id, editorState, renderingEngine
    } = this.state;

    if (loading) return <LoadingPlaceholder />;
    return (
      <div className="react-editor">
        <MuiThemeProvider theme={theme}>
          <Appbar
            handleSave={this.handleSave}
            handleChange={this.handleChange}
            id={id}
            language={language}
            renderingEngine={renderingEngine}
            goBack={this.goBack}
          />

          <EditorOuterWrapper>
            <EditorWrapper className="is-font-monospace">
              <NumberTitleInput
                number={String(number)}
                title={title}
                slug={slug}
                handleChange={this.handleChange}
                handleSlugify={this.handleSlugify}
                errorList={errors}
              />
              <Editor
                editorState={editorState}
                onChange={this.onChange}
              />
            </EditorWrapper>
          </EditorOuterWrapper>
          <StatusBar open={this.state.messageOpen}
            handleCloseSnackbar={this.handleCloseSnackbar}
            {...this.state.message} />
        </MuiThemeProvider>
      </div>
    );
  };
};

export default MarkdownEditor;
