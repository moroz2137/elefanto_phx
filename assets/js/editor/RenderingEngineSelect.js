import React from 'react';
import styled from 'styled-components';
import _ from 'lodash';
import { Typography, Tooltip, List, Button, Menu, MenuItem } from '@material-ui/core';

const options = ['markdown', 'textile', 'legacy'];

const StyledList = styled(List)`
  color: #fff;
  background-color: inherit;
  box-shadow: none;
`;

class RenderingEngineSelect extends React.Component {
  state = {
    anchorEl: null,
  };

  handleClickListItem = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  }

  handleMenuItemClick = (value) => {
    const fakeEvent = { target: { name: 'renderingEngine', value: value } };
    this.props.handleChange(fakeEvent);
    this.setState({ anchorEl: null });
  }

  handleClose = () => {
    this.setState({ anchorEl: null });
  }

  render() {
    const { anchorEl } = this.state;
    const { renderingEngine } = this.props;
    return (
      <div>
        <Tooltip title="Rendering engine">
          <Button color="inherit"
            onClick={this.handleClickListItem}
          >
            {_.capitalize(renderingEngine)}
          </Button>
        </Tooltip>
        <Menu
          id="language-selector"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          {_.map(options, value => (
            <MenuItem
              key={value}
              selected={value === renderingEngine}
              onClick={() => this.handleMenuItemClick(value)}
            >
              {_.capitalize(value)}
            </MenuItem>)
          )
          }
        </Menu>
      </div>
    )
  }
}

export default RenderingEngineSelect;