import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import { green, deepOrange } from '@material-ui/core/colors';
import { Typography, IconButton, Snackbar, SnackbarContent } from '@material-ui/core';

const styles1 = theme => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: deepOrange[600],
    display: 'grid',
    gridTemplateColumns: '1fr auto'
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
  },
  warning: {
    backgroundColor: deepOrange[600],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
});

const Message = ({ message, variant, className }) => {
  return (<div className={className}>
    {variant === 'error' ?
      <Typography
        variant="caption"
        color="inherit"
        style={{ padding: '0 0 7px 0' }}>
        The following errors occurred:
    </Typography> : ''}
    <Typography variant="body1" color="inherit">{message}</Typography>
  </div>);
}

function MySnackbarContent(props) {
  const { classes, className, message, onClose, variant, ...other } = props;
  // const Icon = variantIcon[variant];

  return (
    <SnackbarContent
      className={classNames(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          {/* <Icon className={classNames(classes.icon, classes.iconVariant)} /> */}
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  );
}

MySnackbarContent.propTypes = {
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  message: PropTypes.node,
  onClose: PropTypes.func,
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
};

const MySnackbarContentWrapper = withStyles(styles1)(MySnackbarContent);

const styles2 = theme => ({
  margin: {
    margin: theme.spacing.unit,
  },
});

class CustomizedSnackbars extends React.Component {
  state = {
    open: false,
  };

  render() {
    const { classes, variant, open, text, handleCloseSnackbar } = this.props;

    return (
      <Snackbar
        anchorOrigin={{ horizontal: 'center', vertical: 'bottom' }}
        open={open}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          variant={variant}
          className={classes.margin}
          onClose={handleCloseSnackbar}
          message={<Message message={text} variant={variant} />}
        />
      </Snackbar>
    );
  }
}

CustomizedSnackbars.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles2)(CustomizedSnackbars);
