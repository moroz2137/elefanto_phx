import React from 'react';
import styled from 'styled-components';
import _ from 'lodash';
import { Menu, MenuItem, List, Button, Typography, Tooltip } from '@material-ui/core';

const options = {
  en: 'English',
  pl: 'Polish',
  zh: 'Chinese',
  ru: 'Russian'
};

const StyledList = styled(List)`
  color: #fff;
  background-color: inherit;
  box-shadow: none;
`;

class LanguageSelect extends React.Component {
  state = {
    anchorEl: null,
  };

  handleClickListItem = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  }

  handleMenuItemClick = (key) => {
    const fakeEvent = { target: { name: 'language', value: key } };
    this.props.handleChange(fakeEvent);
    this.setState({ anchorEl: null });
  }

  handleClose = () => {
    this.setState({ anchorEl: null });
  }

  render() {
    const { anchorEl } = this.state;
    const { language } = this.props;
    return (
      <div>
        <Tooltip title="Post language">
          <Button
            aria-label="Post language"
            color="inherit"
            onClick={this.handleClickListItem}
          >
            <Typography variant="body2" color="inherit">
              {_.upperCase(language)}
            </Typography>
          </Button>
        </Tooltip>

        <Menu
          id="language-selector"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          {_.map(options, (val, key) => (
            <MenuItem
              key={key}
              selected={key === language}
              onClick={() => this.handleMenuItemClick(key)}
            >
              {options[key]}
            </MenuItem>)
          )
          }
        </Menu>
      </div>
    )
  }
}

export default LanguageSelect;