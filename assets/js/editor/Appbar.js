import React from 'react';
import PropTypes from 'prop-types';
import { AppBar, IconButton, Toolbar, Tooltip } from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import ArrowBack from '@material-ui/icons/ArrowBack';
import LanguageSelect from './LanguageSelect';
import RenderingEngineSelect from './RenderingEngineSelect';
import styled from 'styled-components';

const AppBarLeft = styled.div`
  flex: 1;
`;

export default class Appbar extends React.Component {
  static propTypes = {
    handleSave: PropTypes.func,
    handleChange: PropTypes.func,
    goBack: PropTypes.func,
    language: PropTypes.string,
    renderingEngine: PropTypes.string
  };

  render() {
    const { language, renderingEngine, handleSave, goBack, handleChange } = this.props;
    return <AppBar position="static">
      <Toolbar>
        <AppBarLeft>
          <Tooltip title="Return to post">
            <IconButton onClick={goBack} color="inherit">
              <ArrowBack />
            </IconButton>
          </Tooltip>
          <Tooltip title="Save post">
            <IconButton onClick={handleSave} color="inherit">
              <SaveIcon />
            </IconButton>
          </Tooltip>
        </AppBarLeft>
        <LanguageSelect language={language} handleChange={handleChange} />
        <RenderingEngineSelect renderingEngine={renderingEngine} handleChange={handleChange} />
      </Toolbar>
    </AppBar>
  }
}
