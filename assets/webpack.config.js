const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const staticDir = path.join(__dirname, '.');
const destDir = path.join(__dirname, '../priv/static');
const publicPath = '/';

module.exports = (env, options) => ({
  mode: 'development',
  entry: {
    app: `${staticDir}/css/app.sass`,
    editor: ['babel-polyfill', `${staticDir}/js/editor.js`],
    site: ['babel-polyfill', `${staticDir}/js/site.js`, `${staticDir}/js/comments.js`]
  },
  output: {
    path: destDir,
    filename: "js/[name].js",
    publicPath
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /.(css|scss|sass)$/,
        exclude: /node_modules/,
        use: ['style-loader', MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      {
        test: /.(jpe?g|png)$/,
        use: [
          'url-loader?limit=10000',
          {
            loader: 'img-loader',
            options: {
              plugins: [
                require('imagemin-mozjpeg')({
                  progressive: true,
                  arithmetic: false
                }),
                require('imagemin-pngquant')({
                  floyd: 0.5,
                  speed: 2
                })
              ]
            }
          }]
      },
      {
        test: /\.svg$/,
        loader: 'svg-inline-loader'
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({ filename: './css/app.css' }),
    new CopyWebpackPlugin([{ from: 'static/', to: '../static' }])
  ]
});

if (process.env.NODE_ENV === "production") {
  module.exports.mode = 'production';
}