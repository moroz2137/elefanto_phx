defmodule Elefanto.Posts.Comment do
  use Ecto.Schema
  import Ecto.Changeset
  alias Elefanto.Posts.Post

  schema "comments" do
    field(:body, :string)
    field(:signature, :string)
    field(:website, :string)
    field(:ip, EctoNetwork.INET)
    belongs_to(:post, Post)

    timestamps(type: :utc_datetime)
  end

  def new_changeset(comment, attrs) do
    comment
    |> cast(attrs, [:body, :signature])
  end

  @doc false
  def changeset(comment, attrs) do
    comment
    |> new_changeset(attrs)
    |> strip_tags
    |> validate_required([:body, :signature])
    |> validate_length(:body, min: 10, max: 1000)
    |> unique_constraint(:body,
      name: :comments_body_post_id_index,
      message: "You cannot post the same comment twice for the same post."
    )
  end

  defp strip_tags(changeset) do
    case get_change(changeset, :body) do
      nil ->
        changeset

      text ->
        {:safe, sanitized_text} = PhoenixHtmlSanitizer.Helpers.strip_tags(text)

        changeset
        |> put_change(:body, sanitized_text)
    end
  end

  @spec set_post(Ecto.Changeset.t(), Elefanto.Posts.Post.t()) :: Ecto.Changeset.t()
  def set_post(changeset, %Post{} = post) do
    put_assoc(changeset, :post, post)
  end
end
