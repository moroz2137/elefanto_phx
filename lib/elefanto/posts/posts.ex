defmodule Elefanto.Posts do
  @moduledoc """
  The Posts context.
  """

  import Ecto.Query, warn: false
  alias Elefanto.Repo
  alias Ecto.Changeset

  alias Elefanto.Posts.{Post, Comment}

  @doc """
  Returns the list of posts.

  ## Examples

      iex> list_posts()
      [%Post{}, ...]

  """
  def list_posts() do
    Repo.all(from(p in Post, order_by: [desc: :id]))
  end

  @doc """
  Fetches and paginates posts based on request params.
  If the second argument, `published_only`, is true
  (by default), it only returns those posts that are
  already published.
  """
  def paginate_posts(params, published_only \\ true) do
    from(p in Post, order_by: [desc: :number])
    |> Post.filter_published(published_only)
    |> Repo.paginate(params)
  end

  @doc """
  Gets a single post.

  Raises `Ecto.NoResultsError` if the Post does not exist.

  ## Examples

      iex> get_post!(123)
      %Post{}

      iex> get_post!(456)
      ** (Ecto.NoResultsError)

  """
  def get_post!(id), do: Repo.get!(Post, id)

  def get_post_by_slug(slug) when is_bitstring(slug) do
    case Repo.get_by(Post, slug: slug) do
      %Post{} = post ->
        post =
          post
          |> Post.preload_comments()

        {:ok, post}

      nil ->
        {:error, :not_found}
    end
  end

  @doc """
  Spawns a task that increments `view_count` on the `Post` and
  returns the `Post`.
  """
  @spec increment_view_count(Elefanto.Posts.Post.t()) :: Elefanto.Posts.Post.t()
  def increment_view_count(%Post{} = post) do
    Task.start(fn ->
      from(p in Post, where: p.id == ^post.id)
      |> Repo.update_all(inc: [view_count: 1])
    end)

    post
  end

  @doc """
  Creates a post.

  ## Examples

      iex> create_post(%{field: value})
      {:ok, %Post{}}

      iex> create_post(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_post(attrs \\ %{}) do
    %Post{}
    |> Post.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a post.

  ## Examples

      iex> update_post(post, %{field: new_value})
      {:ok, %Post{}}

      iex> update_post(post, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_post(%Post{} = post, attrs) do
    changeset = Post.changeset(post, attrs)

    case Repo.update(changeset) do
      {:ok, post} ->
        clear_post_cache(post)
        {:ok, post}

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  def clear_post_cache(%Post{slug: slug, id: id}) do
    ConCache.delete(:post_cache, id)
  end

  @doc """
  Publishes a post.
  """
  def publish_post(%Post{published_at: nil} = post) do
    post
    |> Changeset.change()
    |> Changeset.put_change(:published_at, DateTime.utc_now())
    |> Repo.update()
  end

  def publish_post(_), do: {:error, :already_published}

  def unpublish_post(%Post{published_at: nil}), do: {:error, :not_published}

  def unpublish_post(%Post{published_at: published_at} = post) when not is_nil(published_at) do
    post
    |> Changeset.change()
    |> Changeset.put_change(:published_at, nil)
    |> Repo.update()
  end

  @doc """
  Deletes a Post.

  ## Examples

      iex> delete_post(post)
      {:ok, %Post{}}

      iex> delete_post(post)
      {:error, %Ecto.Changeset{}}

  """
  def delete_post(%Post{} = post) do
    Repo.delete(post)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking post changes.

  ## Examples

      iex> change_post(post)
      %Ecto.Changeset{source: %Post{}}

  """
  def change_post(%Post{} = post) do
    Post.changeset(post, %{})
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking comment changes.
  """
  def change_comment(%Comment{} = comment) do
    Comment.new_changeset(comment, %{})
  end

  def create_comment(%Post{} = post, params \\ %{}) do
    %Comment{}
    |> Comment.changeset(params)
    |> Comment.set_post(post)
    |> Repo.insert()
  end
end
