defmodule Elefanto.Posts.Post do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  alias Elefanto.Repo
  alias Elefanto.Posts.Comment

  @required_fields ~W[title body language number slug]a
  @optional_fields ~W[user_id rendering_engine]a
  @rendering_engines ~W[textile markdown legacy]

  schema "posts" do
    field(:body, :string)
    field(:language, :string)
    field(:title, :string)
    field(:slug, :string)
    field(:number, :float)
    field(:rendering_engine, :string, default: "markdown")
    field(:user_id, :integer)
    field(:published_at, :utc_datetime)
    field(:view_count, :integer, default: 0)
    has_many(:comments, Comment)

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(post, attrs) do
    post
    |> cast(attrs, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> validate_inclusion(:rendering_engine, @rendering_engines)
    |> unique_constraint(:slug)
  end

  @doc """
  Filters unpublished posts. Used in Posts.paginate_posts/2
  to restrict anonymous users from reading unpublished articles.
  """
  def filter_published(scope), do: filter_published(scope, true)
  def filter_published(scope, false), do: scope

  def filter_published(scope, true) do
    from(p in scope, where: not is_nil(p.published_at))
  end

  def preload_comments(post_or_posts) do
    post_or_posts
    |> Repo.preload(comments: from(c in Comment, order_by: [desc: c.inserted_at]))
  end
end

defimpl Phoenix.Param, for: Elefanto.Posts.Post do
  def to_param(%{slug: slug}), do: slug
end
