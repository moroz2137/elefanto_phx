defmodule Elefanto.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias Elefanto.Accounts.Encryption

  schema "users" do
    field(:email, :string)
    field(:password_hash, :string)

    field(:password, :string, virtual: true)
    field(:password_confirmation, :string, virtual: true)

    timestamps()
  end

  @optional_fields ~W[password password_confirmation]a
  @required_fields ~W[email]a

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, @optional_fields ++ @required_fields)
    |> validate_required(@required_fields)
    |> validate_email
    |> validate_password
    |> encrypt_password
  end

  def registration_changeset(user, attrs) do
    changeset(user, attrs)
    |> validate_required(:password)
  end

  defp validate_email(changeset) do
    changeset
    |> validate_format(:email, ~r/\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i)
    |> unique_constraint(:email)
    |> downcase_email
  end

  def validate_password(changeset) do
    changeset
    |> validate_length(:password, min: 6)
    |> validate_confirmation(:password)
  end

  defp downcase_email(changeset) do
    case get_change(changeset, :email) do
      nil ->
        changeset

      email ->
        put_change(changeset, :email, String.downcase(email))
    end
  end

  defp encrypt_password(changeset) do
    password = get_change(changeset, :password)

    if password do
      encrypted_password = Encryption.hash_password(password)
      put_change(changeset, :password_hash, encrypted_password)
    else
      changeset
    end
  end
end
