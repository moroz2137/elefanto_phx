defmodule Elefanto.Accounts.Auth do
  alias Elefanto.Accounts.{Encryption, User}
  alias Comeonin.Argon2

  def login(%{"email" => email, "password" => password}) do
    user = Elefanto.Accounts.get_user_by(email: String.downcase(email))

    case authenticate(user, password) do
      true -> {:ok, user}
      _ -> :error
    end
  end

  defp authenticate(nil, _), do: Encryption.dummy_checkpw()

  defp authenticate(user = %User{password_hash: password_hash}, password) do
    Encryption.validate_password(password, password_hash)
  end
end
