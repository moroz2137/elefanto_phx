defmodule Elefanto.Uploads do
  @moduledoc """
  The Uploads context.
  """

  import Ecto.Query, warn: false
  alias Elefanto.Repo
  alias Elefanto.Uploads.ImageUploader

  alias Elefanto.Uploads.File

  @doc """
  Returns the list of files.

  ## Examples

      iex> list_files()
      [%File{}, ...]

  """
  def list_files do
    Repo.all(File)
  end

  def get_files_by_slugs([]), do: []

  def get_files_by_slugs(slugs) when is_list(slugs) do
    from(f in File, where: f.slug in ^slugs, select: {f.slug, f})
    |> Repo.all()
  end

  @doc """
  Gets a single file.

  Raises `Ecto.NoResultsError` if the File does not exist.

  ## Examples

      iex> get_file!(123)
      %File{}

      iex> get_file!(456)
      ** (Ecto.NoResultsError)

  """
  def get_file!(id), do: Repo.get!(File, id)

  @spec get_file_by(Keyword.t()) :: {:error, :not_found} | {:ok, Elefanto.Uploads.File.t()}
  def get_file_by(params) do
    case Repo.get_by(File, params) do
      %File{} = file ->
        {:ok, file}

      nil ->
        {:error, :not_found}
    end
  end

  @doc """
  Creates a file.

  ## Examples

      iex> create_file(%{field: value})
      {:ok, %File{}}

      iex> create_file(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_file(attrs \\ %{}) do
    %File{}
    |> File.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Uploads file uploaded in the browser to AWS and saves its path
  in the database.
  """
  def upload_file(attrs = %{"filename" => %Plug.Upload{} = upload}) do
    changeset =
      %File{}
      |> File.changeset(attrs)

    case changeset.valid? do
      true ->
        {:ok, filename} = ImageUploader.store(upload)

        File.put_filename(changeset, filename)
        |> Repo.insert()

      false ->
        {:error, changeset}
    end
  end

  @doc """
  Updates a file.

  ## Examples

      iex> update_file(file, %{field: new_value})
      {:ok, %File{}}

      iex> update_file(file, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_file(%File{} = file, attrs) do
    file
    |> File.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a File.

  ## Examples

      iex> delete_file(file)
      {:ok, %File{}}

      iex> delete_file(file)
      {:error, %Ecto.Changeset{}}

  """
  def delete_file(%File{} = file) do
    Repo.delete(file)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking file changes.

  ## Examples

      iex> change_file(file)
      %Ecto.Changeset{source: %File{}}

  """
  def change_file(%File{} = file) do
    File.changeset(file, %{})
  end
end
