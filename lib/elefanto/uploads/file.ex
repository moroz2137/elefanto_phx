defmodule Elefanto.Uploads.File do
  use Ecto.Schema
  import Ecto.Changeset

  schema "files" do
    field(:filename, :string)
    field(:slug, :string)
    field(:description, :string)

    timestamps()
  end

  @required_fields ~W[slug]a
  @optional_fields ~W[description]a

  @doc false
  def changeset(file, attrs) do
    file
    |> cast(attrs, @optional_fields ++ @required_fields)
    |> validate_required(@required_fields)
    |> unique_constraint(:slug)
    |> escape_slug
  end

  @spec put_filename(Ecto.Changeset.t(), String.t()) :: Ecto.Changeset.t()
  def put_filename(changeset, filename) do
    put_change(changeset, :filename, filename)
  end

  defp escape_slug(changeset) do
    case get_change(changeset, :slug) do
      nil ->
        changeset

      slug ->
        slug = Slug.slugify(slug, ignore: "-")
        put_change(changeset, :slug, slug)
    end
  end
end

defimpl Phoenix.Param, for: Elefanto.Uploads.File do
  def to_param(%{slug: slug}), do: slug
end
