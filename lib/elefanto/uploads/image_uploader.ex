defmodule Elefanto.Uploads.ImageUploader do
  use Arc.Definition

  @versions [:original, :mobile, :tablet, :desktop]
  @acl :public_read
  @extension_whitelist ~w(.png .jpg .jpeg .gif .svg)

  @mobile_width 480
  @tablet_width 720
  @desktop_width 960

  # Override the bucket on a per definition basis:
  # def bucket do
  #   :custom_bucket_name
  # end

  # Whitelist file extensions:
  def validate({file, _}) do
    Enum.member?(@extension_whitelist, extension(file))
  end

  defp extension(%{file_name: file_name}), do: Path.extname(file_name)

  def transform(:original, _), do: :noaction

  def transform(version, {file, _}) do
    if Enum.member?(~w(.jpg .jpeg), extension(file)) do
      size =
        case version do
          :mobile -> @mobile_width
          :tablet -> @tablet_width
          :desktop -> @desktop_width
        end

      {:convert, conversion_command(size)}
    else
      :noaction
    end
  end

  defp conversion_command(size) do
    "-filter Triangle -define filter:support=2 -thumbnail #{size} -unsharp 0.25x0.25+8+0.065 -dither None -posterize 136 -quality 82 -define jpeg:fancy-upsampling=off -define png:compression-filter=5 -define png:compression-level=9 -define png:compression-strategy=1 -define png:exclude-chunk=all -interlace none -colorspace sRGB -strip"
  end

  # Define a thumbnail transformation:
  # def transform(:thumb, _) do
  #   {:convert, "-strip -thumbnail 250x250^ -gravity center -extent 250x250 -format png", :png}
  # end

  def filename(:original, {file, _scope}) do
    strip_extension(file.file_name)
  end

  # Override the persisted filenames:
  def filename(version, {file, _scope}) do
    file_name = strip_extension(file.file_name)
    "#{version}_#{file_name}"
  end

  defp strip_extension(filename) do
    Path.basename(filename, Path.extname(filename))
  end

  def storage_dir(_, _), do: "uploads"

  # Provide a default URL if there hasn't been a file uploaded
  # def default_url(version, scope) do
  #   "/images/avatars/default_#{version}.png"
  # end

  # Specify custom headers for s3 objects
  # Available options are [:cache_control, :content_disposition,
  #    :content_encoding, :content_length, :content_type,
  #    :expect, :expires, :storage_class, :website_redirect_location]
  #
  def s3_object_headers(_version, {file, _scope}) do
    [content_type: MIME.from_path(file.file_name)]
  end
end
