defmodule Elefanto.Application do
  use Application

  @con_cache_conf Application.fetch_env!(:con_cache, :config)

  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec

    # Define workers and child supervisors to be supervised
    children = [
      # Start the Ecto repository
      supervisor(Elefanto.Repo, []),
      # Start the endpoint when the application starts
      supervisor(ElefantoWeb.Endpoint, []),
      {ConCache, @con_cache_conf}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Elefanto.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    ElefantoWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
