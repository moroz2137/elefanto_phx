defmodule ElefantoWeb.Router do
  use ElefantoWeb, :router
  use Plug.ErrorHandler

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
    plug(ElefantoWeb.SetLocale)
    plug(ElefantoWeb.Plugs.FetchUser)
  end

  pipeline :api do
    plug(:accepts, ["json", "json-api"])
    # plug(JaSerializer.ContentTypeNegotiation)
    plug(JaSerializer.Deserializer)
  end

  scope "/api", ElefantoWeb do
    pipe_through(:api)

    resources("/posts", PostJsonController, except: [:new, :edit])
    resources("/comments", CommentController, only: [:create, :delete])
    get("/slugify_title", PostJsonController, :slugify)
  end

  scope "/", ElefantoWeb do
    pipe_through(:browser)

    get("/", PageController, :index)
    forward("/posts", Redirector)

    resources("/blog", PostController, except: [:create, :update], param: "slug")

    patch("/blog/:slug/publish", PostController, :publish, as: :post)
    patch("/blog/:slug/unpublish", PostController, :unpublish, as: :post)

    resources("/files", FileController, only: [:create, :show], param: "slug")
    get("/login", SessionController, :new)
    post("/login", SessionController, :create)
    get("/logout", SessionController, :delete)
  end

  defp handle_errors(conn, opts) do
    ElefantoWeb.RollbarReporter.handle_errors(conn, opts)
  end
end
