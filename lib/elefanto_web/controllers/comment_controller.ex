defmodule ElefantoWeb.CommentController do
  use ElefantoWeb, :controller

  @enable_recaptcha Application.fetch_env!(:elefanto, :enable_recaptcha)

  plug(:verify_captcha, [enable: @enable_recaptcha] when action == :create)
  plug(:find_post when action == :create)

  alias Elefanto.Posts

  def create(conn, %{"comment" => comment_params}) do
    case Posts.create_comment(conn.assigns.post, comment_params) do
      {:ok, comment} ->
        conn
        |> put_status(:created)
        |> render("show.json-api", data: comment)

      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render("errors.json-api", data: changeset)
    end
  end

  defp verify_captcha(conn, enable: false), do: conn

  defp verify_captcha(%Plug.Conn{body_params: params} = conn, _) do
    %{"g-recaptcha-response" => captcha} = params

    case Recaptcha.verify(captcha) do
      {:ok, _response} ->
        conn

      {:error, _errors} ->
        conn
        |> send_resp(204, "")
        |> halt()
    end
  end

  defp find_post(conn, _) do
    %Plug.Conn{params: %{"post_id" => id}} = conn

    assign(conn, :post, Posts.get_post!(id))
  end
end
