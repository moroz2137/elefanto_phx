defmodule ElefantoWeb.PageController do
  use ElefantoWeb, :controller

  def index(conn, _params) do
    case Gettext.get_locale(ElefantoWeb.Gettext) do
      "zh" ->
        render(conn, "index.zh.html")

      _ ->
        render(conn, "index.html")
    end
  end
end
