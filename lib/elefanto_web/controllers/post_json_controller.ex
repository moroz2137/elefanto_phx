defmodule ElefantoWeb.PostJsonController do
  use ElefantoWeb, :controller

  alias Elefanto.Posts

  plug(ElefantoWeb.TokenAuth)

  def create(conn, %{"post" => post_params}) do
    case Posts.create_post(post_params) do
      {:ok, post} ->
        conn
        |> put_status(:created)
        |> render("show.json-api", data: post)

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render("errors.json-api", data: changeset)
    end
  end

  def slugify(conn, %{"number" => number, "title" => title}) do
    slug =
      [number, title]
      |> Enum.join(" ")
      |> Slug.slugify()

    json(conn, %{data: %{slug: slug}})
  end

  def show(conn, %{"id" => id}) do
    post = Posts.get_post!(id)
    render(conn, "show.json-api", data: post)
  end

  def update(conn, %{"id" => id, "post" => post_params}) do
    post = Posts.get_post!(id)

    case Posts.update_post(post, post_params) do
      {:ok, _post} ->
        conn
        |> send_resp(:no_content, "")

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, ElefantoWeb.ChangesetView, "error.json", changeset: changeset)
    end
  end
end
