defmodule ElefantoWeb.FileController do
  use ElefantoWeb, :controller

  alias Elefanto.Uploads
  alias Elefanto.Uploads.File
  action_fallback(ElefantoWeb.FallbackController)

  plug(ElefantoWeb.Plugs.RestrictAccess when action != :show)

  def create(conn, %{"file" => file_params}) do
    case Uploads.upload_file(file_params) do
      {:ok, file} ->
        conn
        |> put_flash(:info, "The file has been uploaded and will be processed asynchronously.")
        |> redirect(to: file_path(conn, :show, file))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"slug" => slug}) do
    case Uploads.get_file_by(slug: slug) do
      {:ok, file} ->
        conn
        |> render("show.html", file: file, slug: slug)

      {:error, :not_found} ->
        case conn.assigns[:current_user] do
          nil ->
            render(conn, "show.html", file: nil, slug: slug)

          %Elefanto.Accounts.User{} ->
            changeset = Uploads.change_file(%File{slug: slug})

            conn
            |> render("new.html", slug: slug, changeset: changeset)
        end
    end
  end
end
