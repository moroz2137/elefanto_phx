defmodule ElefantoWeb.SessionController do
  use ElefantoWeb, :controller

  alias Elefanto.Repo
  alias Elefanto.Accounts.{User, Auth}

  def new(conn, _params) do
    render(conn, "new.html")
  end

  def create(conn, params) do
    case Auth.login(params) do
      {:ok, user} ->
        conn
        |> put_session(:user_id, user.id)
        |> put_flash(:info, "Logged in.")
        |> redirect(to: post_path(conn, :index))

      :error ->
        conn
        |> put_flash(:error, "Authentication failed for the given email and password pair.")
        |> render("new.html")
    end
  end

  def delete(conn, _params) do
    case get_session(conn, :user_id) do
      nil ->
        conn
        |> put_flash(:info, "You are already signed out.")
        |> redirect(to: "/")

      _ ->
        conn
        |> clear_session()
        |> put_flash(:success, "You have been signed out.")
        |> redirect(to: "/")
    end
  end
end
