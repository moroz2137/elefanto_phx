defmodule ElefantoWeb.FallbackController do
  use Phoenix.Controller
  alias ElefantoWeb.ErrorView

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> render(ErrorView, "404.html")
    |> halt()
  end
end
