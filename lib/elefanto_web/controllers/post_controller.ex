defmodule ElefantoWeb.PostController do
  use ElefantoWeb, :controller

  alias Elefanto.Posts

  plug(:put_layout, "full.html" when action in [:new, :edit])

  plug(:get_post when action not in [:index, :new])
  plug(:increment_view_count when action == :show)
  plug(:put_user_token when action in [:new, :edit])

  plug(
    ElefantoWeb.Plugs.RestrictAccess,
    [fallback_path: "/blog"] when action not in [:index, :show]
  )

  @doc """
  Call controller actions with the post as the third param
  for singular actions. The post has to be stored in
  conn.assigns.post beforehand.
  """
  def action(conn, _) do
    case action_name(conn) do
      action when action in [:index, :new] ->
        apply(__MODULE__, action, [conn, conn.params])

      action ->
        args = [conn, conn.params, conn.assigns[:post]]
        apply(__MODULE__, action, args)
    end
  end

  def index(conn, params) do
    only_published = !conn.assigns.current_user
    page = Posts.paginate_posts(params, only_published)
    render(conn, "index.html", posts: page.entries, page: page, title: "Blog Archive")
  end

  def new(conn, _params) do
    render(conn, "editor.html")
  end

  def show(conn, _params, post) do
    conn
    |> assign(:title, ElefantoWeb.PostView.pretty_title(post))
    |> assign_comments_props(post.comments)
    |> render("show.html")
  end

  defp assign_comments_props(conn, comments) do
    comments =
      Phoenix.View.render_to_string(ElefantoWeb.CommentView, "comments.json", comments: comments)

    assign(conn, :comments_json, comments)
  end

  def edit(conn, _params, post) do
    render(conn, "editor.html", props: %{id: post.id})
  end

  def delete(conn, _params, post) do
    case Posts.delete_post(post) do
      {:ok, _} ->
        conn
        |> put_flash(:success, "The post has been deleted.")
        |> redirect(to: post_path(conn, :index))

      {:error, _} ->
        conn
        |> put_flash(:error, "An error occurred while processing your request.")
        |> redirect(to: post_path(conn, :show, post))
    end
  end

  def publish(conn, _params, post) do
    conn =
      case Posts.publish_post(post) do
        {:ok, _post} ->
          conn
          |> put_flash(:success, "The post has been published.")

        {:error, :already_published} ->
          conn
          |> put_flash(:error, "This post has already been published before.")
      end

    redirect(conn, to: post_path(conn, :show, post))
  end

  def unpublish(conn, _params, post) do
    conn =
      case Posts.unpublish_post(post) do
        {:ok, _post} ->
          conn
          |> put_flash(:success, "The post has been unpublished.")

        {:error, :not_published} ->
          conn
          |> put_flash(:error, "This post has not been published yet.")
      end

    redirect(conn, to: post_path(conn, :show, post))
  end

  defp get_post(%Plug.Conn{params: %{"slug" => slug}} = conn, _) do
    case Posts.get_post_by_slug(slug) do
      {:ok, post} ->
        assign(conn, :post, post)

      other ->
        ElefantoWeb.FallbackController.call(conn, other)
    end
  end

  defp put_user_token(conn, _) do
    case conn.assigns.current_user do
      %Elefanto.Accounts.User{} = user ->
        token = ElefantoWeb.TokenAuth.issue_for(user)
        assign(conn, :user_token, token)

      _ ->
        conn
    end
  end

  defp increment_view_count(conn, _) do
    case Browser.bot?(conn) do
      true ->
        conn

      false ->
        Posts.increment_view_count(conn.assigns.post)
        conn
    end
  end
end
