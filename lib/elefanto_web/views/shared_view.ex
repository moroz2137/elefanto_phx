defmodule ElefantoWeb.SharedView do
  use ElefantoWeb, :view
  import Phoenix.Controller, only: [current_url: 1, current_url: 2]

  def readable_model_name(%Ecto.Changeset{} = changeset) do
    changeset.data.__struct__
    |> Atom.to_string()
    |> String.split(".")
    |> List.last()
    |> String.downcase()
  end
end
