defmodule ElefantoWeb.PostView do
  use ElefantoWeb, :view
  alias Elefanto.Posts.Post
  alias Elefanto.Uploads
  alias ElefantoWeb.{FileView, CommentView}
  import Scrivener.HTML

  def render("show.json", %{post: %Post{} = post}) do
    post
  end

  def button_back do
    link to: "/blog", class: "btn-flat purple-text text-darken-2 posts-button-back" do
      [icon("arrow_back", "left"), " ", gettext("Back")]
    end
  end

  @spec readable_number(number()) :: String.t()
  def readable_number(number) do
    case trunc(number) == number do
      true ->
        trunc(number)
        |> Integer.to_string()

      false ->
        number
        |> Float.to_string()
        |> String.replace(".", ",", global: false)
    end
  end

  @spec pretty_title(Elefanto.Posts.Post.t()) :: [...]
  def pretty_title(%Post{number: number, title: title}) do
    [readable_number(number), ". ", title]
  end

  def post_details(post) do
    content_tag(:div, class: "post-details") do
      [date_span(post), language_span(post)]
    end
  end

  defp publish_post_button(conn, %Post{published_at: nil} = post) do
    link to: post_path(conn, :publish, post),
         class: "btn-flat waves-effect waves-light purple-text text-lighten-2",
         method: :patch do
      [icon("publish", "left"), " ", gettext("Publish")]
    end
  end

  defp publish_post_button(conn, %Post{} = post) do
    link to: post_path(conn, :unpublish, post),
         class: "btn-flat waves-effect waves-light orange-text text-darken-2",
         method: :patch do
      [icon("hot_tub", "left"), " ", gettext("Unpublish")]
    end
  end

  def edit_post_button(conn, %Post{} = post) do
    link to: post_path(conn, :edit, post),
         class: "btn-flat waves-effect blue-text text-darken-1" do
      [icon("edit", "left"), " ", gettext("Edit")]
    end
  end

  def actions(conn, post) do
    content_tag(:div, class: "post-actions") do
      [
        edit_post_button(conn, post),
        publish_post_button(conn, post)
      ]
    end
  end

  defp view_count(%Post{view_count: view_count}) do
    content_tag(:span, class: "post-view-count") do
      [icon("poll"), " ", ngettext("1 view", "%{count} views", view_count)]
    end
  end

  defp date_span(post) do
    content_tag(:span, class: "post-date") do
      [icon("public"), " ", content_tag(:span, format_date(post), class: "post-date-text")]
    end
  end

  defp language_span(post) do
    content_tag(:span, class: "post-language") do
      [
        icon("language"),
        " ",
        content_tag(:span, language_name(post), class: "post-language-text")
      ]
    end
  end

  def format_date(%Post{published_at: nil}), do: 'Unpublished'

  def format_date(%Post{published_at: date}) do
    Timex.format!(date, gettext("{D} {Mshort}., {YYYY}"))
  end

  def language_name(%Post{language: "pl"}), do: dgettext("languages", "Polish")
  def language_name(%Post{language: "en"}), do: dgettext("languages", "English")
  def language_name(%Post{language: "zh"}), do: dgettext("languages", "Chinese")
  def language_name(%Post{language: "ru"}), do: dgettext("languages", "Russian")
  def language_name(_), do: dgettext("languages", "Other")

  def render_post_body(%Post{id: id, body: body, rendering_engine: "textile"}) do
    ConCache.get_or_store(:post_cache, id, fn -> render_textile(body) end)
  end

  def render_post_body(%Post{body: body, rendering_engine: "legacy"}),
    do: render_legacy_markup(body)

  def render_post_body(%Post{body: body, rendering_engine: "markdown"}) do
    body
    |> Earmark.as_html!()
    |> render_images
    |> raw
  end

  defp render_textile(body) do
    {:ok, html} = Pandex.textile_to_html5(body)
    raw(html)
  end

  @image_pattern ~r/\[\[Image:([\d\w\-]+)\]\]/

  def render_images(body) do
    fragments = Regex.split(@image_pattern, body, include_captures: true)

    files =
      scan_slugs(fragments)
      |> Uploads.get_files_by_slugs()

    fragments
    |> Enum.map(fn fragment ->
      fragment_to_image(fragment, files)
    end)
  end

  defp fragment_to_image(fragment, files) do
    case Regex.run(@image_pattern, fragment, capture: :all_but_first) do
      [slug] ->
        slug_to_image(slug, files)

      nil ->
        fragment
    end
  end

  defp slug_to_image(slug, files) do
    case List.keyfind(files, slug, 0) do
      {_slug, file} ->
        {:safe, list} = render(FileView, "_image.html", file: file)
        list

      _ ->
        {:safe, list} =
          link(["Image:", slug], to: "/files/#{slug}", class: "is-new", title: "Not uploaded")

        list
    end
  end

  defp scan_slugs(list) when is_list(list), do: scan_slugs(list, [])

  defp scan_slugs([], results), do: results

  defp scan_slugs([fragment | rest], results) do
    case Regex.run(@image_pattern, fragment, capture: :all_but_first) do
      nil ->
        scan_slugs(rest, results)

      [slug] ->
        scan_slugs(rest, [slug | results])
    end
  end

  @doc """
  Renders pseudo-Wikicode.
  My first blog was using Mediawiki as backend, so the old posts
  are still written in Wikicode markup.
  """
  def render_legacy_markup(text) do
    text
    |> String.replace(~r/'''([^']+)'''/, "<strong>\\1</strong>")
    |> String.replace(~r/''([^']+)''/, "<em>\\1</em>")
    |> String.replace(~r/===([^=]+)===/, "<h3>\\1</h3>")
    |> String.replace(~r/==([^=]+)==/, "<h2>\\1</h2>")
    |> text_to_html(escape: false, insert_brs: false)
  end
end
