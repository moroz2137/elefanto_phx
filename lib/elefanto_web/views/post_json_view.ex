defmodule ElefantoWeb.PostJsonView do
  use ElefantoWeb, :view
  use JaSerializer.PhoenixView

  attributes([:id, :title, :slug, :number, :body, :language, :rendering_engine])
end
