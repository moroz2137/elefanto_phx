defmodule ElefantoWeb.FileView do
  use ElefantoWeb, :view

  alias Elefanto.Uploads.File
  alias Elefanto.Uploads.ImageUploader, as: Uploader

  @image_sizes "(max-width: 500px) 480px, (max-width: 800px) 640px, 960px"

  def render("_image.html", %{file: nil}), do: nil

  def render("_image.html", %{file: file}) do
    %File{filename: filename} = file

    case Enum.member?(~w(.jpg .jpeg), Path.extname(filename)) do
      true -> render_responsive(file)
      false -> render_static(file)
    end
  end

  def render_responsive(%File{filename: filename, slug: slug} = file) do
    url = Uploader.url(filename)

    content_tag(:div, class: "center-align") do
      link to: "/files/#{slug}" do
        img_tag(url,
          alt: alt(file),
          class: "responsive-img",
          sizes: @image_sizes,
          srcset: srcset(file)
        )
      end
    end
  end

  def render_static(%File{filename: filename, slug: slug} = file) do
    url = Uploader.url(filename)

    content_tag(:div, class: "center-align") do
      link to: "/files/#{slug}" do
        img_tag(url, alt: alt(file), class: "responsive-img")
      end
    end
  end

  defp alt(%File{description: description, filename: filename}) when description in [nil, ""] do
    ["Image: ", filename, " (no description provided)"]
  end

  defp alt(%File{description: description}) do
    ["Image: ", Phoenix.HTML.escape_javascript(description)]
  end

  defp srcset(%File{filename: filename}) do
    sizes = %{mobile: "480", tablet: "720", desktop: "960"}

    [:mobile, :tablet, :desktop]
    |> Enum.map(fn size ->
      filename = Uploader.url(filename, size)
      "#{filename} #{sizes[size]}w"
    end)
    |> Enum.join(", ")
  end
end
