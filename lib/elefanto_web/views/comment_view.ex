defmodule ElefantoWeb.CommentView do
  use ElefantoWeb, :view
  use JaSerializer.PhoenixView
  alias Elefanto.Posts.Comment

  @enable_recaptcha Application.fetch_env!(:elefanto, :enable_recaptcha)
  @recaptcha_site_key Application.fetch_env!(:recaptcha, :public_key)
  @json_attributes [:id, :body, :signature, :inserted_at]

  attributes([:id, :body, :signature, :inserted_at])

  def render("comments.json", %{comments: comments}) do
    %{comments: render_many(comments, __MODULE__, "comment.json")}
  end

  def render("comment.json", %{comment: comment}) do
    Map.take(comment, @json_attributes)
  end

  defp recaptcha_site_key do
    if @enable_recaptcha, do: @recaptcha_site_key, else: ""
  end

  defp should_render_recaptcha, do: @enable_recaptcha

  @spec format_date(Timex.DateTime.t()) :: String.t()
  def format_date(date) do
    date
    |> Timex.to_datetime("Europe/Warsaw")
    |> Timex.format!("{D} {Mshort}., {YYYY} at {h24}:{m} {Zabbr}")
  end
end
