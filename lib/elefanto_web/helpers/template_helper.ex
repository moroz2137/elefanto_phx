defmodule ElefantoWeb.TemplateHelper do
  @doc """
  Generates an <i> tag compliant with the icon syntax of Materialize.css.
  The first argument is the icon name and the second is a string with other class
  names, e.g. `"large"` or `"tiny"`.

  The list of available icons along with the possible class names is available at:
  https://materializecss.com/icons.html
  """
  @spec icon(String.t(), String.t() | nil) :: Phoenix.HTML.safe()
  def icon(name, additional_classes \\ nil) do
    {:safe, ["<i class=\"material-icons #{additional_classes}\">", name, "</i>"]}
  end
end
