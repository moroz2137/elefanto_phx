defmodule ElefantoWeb.Helpers.InputHelper do
  use Phoenix.HTML
  alias Phoenix.HTML.Form

  @doc """
  Generates an input group, with a wrapper compliant with Bulma.io syntax.
  Solution based on http://blog.plataformatec.com.br/2016/09/dynamic-forms-with-phoenix/
  It automatically determines input type (for e-mail, password)
  """
  def input(form, field, opts \\ []) do
    opts = [class: ["input ", state_class(form, field)]] ++ opts

    {type, opts} = get_input_type(form, field, opts)

    content_tag :div, class: "input-field" do
      [
        apply(Form, type, [form, field, opts]),
        label(form, field, humanize(field), label_opts()),
        ElefantoWeb.ErrorHelpers.error_tag(form, field)
      ]
    end
  end

  def submit_button(text \\ "Submit", opts \\ def_submit_opts()) do
    content_tag :div, class: "field" do
      submit(text, opts)
    end
  end

  defp def_submit_opts, do: [class: "button is-primary"]

  defp label_opts, do: [class: "label"]

  defp get_input_type(form, field, opts) do
    type = Keyword.get(opts, :type) || Form.input_type(form, field)
    opts = Keyword.delete(opts, :type)
    {type, opts}
  end

  defp state_class(form, field) do
    cond do
      form.errors[field] -> " is-danger"
      true -> ""
    end
  end
end
