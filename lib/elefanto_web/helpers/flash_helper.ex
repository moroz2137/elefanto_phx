defmodule ElefantoWeb.Helpers.FlashHelper do
  use Phoenix.HTML
  import Phoenix.ConnTest, only: [get_flash: 1]

  def render_flash(conn) do
    conn
    |> get_flash
    |> Enum.map(&message_div/1)
  end

  defp message_div({key, val}) do
    opts = [class: notification_class(key)]
    content_tag(:div, val, opts)
  end

  defp notification_class(flash_key) when is_bitstring(flash_key),
    do: ["card-panel flash is-", flash_key]
end
