defmodule ElefantoWeb.Redirector do
  import Plug.Conn

  @moduledoc """
  Simple plug used to redirect from legacy URIs in the router to newer URLs in order to avoid
  Not Found responses from Google.

  The old Elefanto website used URLs in the format: `/posts/:slug` and I want the blog to be
  available only at `/blog/:slug`.
  """

  @spec init(any()) :: any()
  def init(default), do: default

  @spec call(Plug.Conn.t(), any()) :: Plug.Conn.t()
  def call(%Plug.Conn{request_path: "/posts" <> path} = conn, _) do
    conn
    |> put_status(:moved_permanently)
    |> Phoenix.Controller.redirect(to: "/blog" <> path)
  end

  def call(conn, _), do: conn
end
