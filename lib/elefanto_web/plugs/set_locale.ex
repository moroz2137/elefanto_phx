defmodule ElefantoWeb.SetLocale do
  import Plug.Conn
  def init(options), do: options

  defp extract_locale_from_params(%{params: %{"lang" => lang}}) when is_bitstring(lang), do: lang
  defp extract_locale_from_params(_), do: nil

  defp set_locale(lang) when lang in ~w(zh en) do
    Gettext.put_locale(ElefantoWeb.Gettext, lang)
  end

  defp set_locale(_), do: nil

  def call(conn, _) do
    case extract_locale_from_params(conn) do
      nil ->
        case get_session(conn, :locale) do
          nil ->
            conn

          lang ->
            set_locale(lang)
            conn
        end

      lang ->
        set_locale(lang)
        put_session(conn, :locale, lang)
    end
  end
end
