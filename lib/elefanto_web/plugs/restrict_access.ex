defmodule ElefantoWeb.Plugs.RestrictAccess do
  import Plug.Conn
  import Phoenix.Controller, only: [put_flash: 3, redirect: 2]

  alias Elefanto.Accounts.User

  def init(fallback_path: path), do: [fallback_path: path]
  def init(_opts), do: [fallback_path: "/"]

  def call(conn, fallback_path: fallback_path) do
    case conn.assigns[:current_user] do
      %User{} ->
        conn

      _ ->
        conn
        |> put_flash(:alert, "You are not allowed to see this page.")
        |> redirect(to: fallback_path)
        |> halt()
    end
  end
end
