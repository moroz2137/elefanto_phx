defmodule ElefantoWeb.TokenAuth do
  alias Phoenix.Token
  alias Elefanto.Accounts
  alias Elefanto.Accounts.User
  import Plug.Conn

  @token_salt Application.fetch_env!(:elefanto, :token_salt)
  @max_age 12 * 3600

  @spec init(any()) :: any()
  def init(default), do: default

  @spec call(Plug.Conn.t(), any()) :: Plug.Conn.t()
  def call(conn, _opts) do
    case get_req_header(conn, "authorization") do
      ["Bearer " <> token] ->
        case get_user_by_token(token) do
          %User{} = user ->
            assign(conn, :current_user, user)

          _ ->
            deny_access(conn)
        end

      _ ->
        deny_access(conn)
    end
  end

  defp deny_access(conn) do
    conn
    |> send_resp(401, "")
    |> halt()
  end

  @spec issue_for(Elefanto.Accounts.User.t()) :: String.t()
  def issue_for(%User{id: id}) when not is_nil(id) do
    Token.sign(ElefantoWeb.Endpoint, @token_salt, %{user_id: id}, max_age: @max_age)
  end

  @spec get_user_by_token(String.t() | nil) :: Elefanto.Accounts.User.t() | nil
  def get_user_by_token(token) when token in [nil, ""], do: nil

  def get_user_by_token(token) when is_bitstring(token) do
    case Token.verify(ElefantoWeb.Endpoint, @token_salt, token, max_age: @max_age) do
      {:ok, %{user_id: id}} ->
        Accounts.get_user(id)

      _ ->
        nil
    end
  end
end
