defmodule Elefanto.Factory do
  use ExMachina.Ecto, repo: Elefanto.Repo

  def user_factory do
    %Elefanto.Accounts.User{
      email: "foobar@example.com",
      password: "foobar"
    }
  end

  @spec post_factory() :: Elefanto.Posts.Post.t()
  def post_factory do
    number = :rand.uniform(200)

    %Elefanto.Posts.Post{
      title: sequence(:title, &"Some title #{&1}"),
      number: number,
      body: "Lorem ipsum dolor sit amet",
      language: "en",
      rendering_engine: "markdown",
      slug: "#{number}-some-title-#{number}"
    }
  end

  def make_published(post) do
    %{post | published_at: Timex.now()}
  end

  def published_post do
    build(:post)
    |> make_published
    |> insert
  end
end
