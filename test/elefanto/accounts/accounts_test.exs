defmodule Elefanto.AccountsTest do
  use Elefanto.DataCase

  alias Elefanto.Accounts

  describe "users" do
    alias Elefanto.Accounts.User
    import Elefanto.Factory

    @valid_attrs params_for(:user)
    @update_attrs %{email: "changed@example.com", password: "new_password"}
    @invalid_attrs %{email: nil, password: nil}

    def user_fixture(attrs \\ %{}) do
      user = insert(:user)
      # reload record to get rid of the password attribute
      Accounts.get_user!(user.id)
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Accounts.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = insert(:user)
      actual = Accounts.get_user!(user.id)
      assert actual.id == user.id
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Accounts.create_user(@valid_attrs)
      assert user.email == @valid_attrs.email
      refute is_nil(user.password_hash)
    end

    test "create_user/1 does not create user without email" do
      attrs = params_for(:user, email: nil)
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(attrs)
    end

    test "create_user/1 does not create user with invalid email" do
      attrs = params_for(:user, email: "invalid")
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(attrs)
    end

    test "create_user/1 does not create user without password" do
      attrs = params_for(:user, password: nil)
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(attrs)
    end

    test "create_user/1 does not create user with short password" do
      attrs = params_for(:user, password: "foo")
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(attrs)
    end

    test "create_user/1 does not create user when password confirmation fails" do
      attrs = params_for(:user, password: "foobar", password_confirmation: "not_foobar")
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(attrs)
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = insert(:user)
      old_password_hash = user.password_hash
      assert {:ok, user} = Accounts.update_user(user, @update_attrs)
      assert user.email == "changed@example.com"
      assert user.password_hash != old_password_hash
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_attrs)
      assert user == Accounts.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = insert(:user)
      assert {:ok, %User{}} = Accounts.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = insert(:user)
      assert %Ecto.Changeset{} = Accounts.change_user(user)
    end
  end
end
