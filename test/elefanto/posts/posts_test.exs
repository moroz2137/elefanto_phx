defmodule Elefanto.PostsTest do
  use Elefanto.DataCase

  alias Elefanto.Posts

  describe "posts" do
    alias Elefanto.Posts.Post

    import Elefanto.Factory

    @valid_attrs params_for(:post)
    @update_attrs %{
      body: "some updated body",
      language: "some updated language",
      title: "some updated title",
      user_id: 43
    }
    @invalid_attrs %{body: nil, language: nil, title: nil, user_id: nil}

    test "list_posts/0 returns all posts" do
      post = insert(:post)
      assert Posts.list_posts() == [post]
    end

    test "get_post!/1 returns the post with given id" do
      post = insert(:post)
      assert Posts.get_post!(post.id) == post
    end

    test "create_post/1 with valid data creates a post" do
      attrs =
        params_for(:post, title: "Jan Paweł II", language: "en", user_id: 2137, body: "Papierzak")

      assert {:ok, %Post{} = post} = Posts.create_post(attrs)
      assert post.language == "en"
      assert post.body == "Papierzak"
      assert post.title == "Jan Paweł II"
      assert post.user_id == 2137
    end

    test "create_post/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Posts.create_post(@invalid_attrs)
    end

    test "update_post/2 with valid data updates the post" do
      post = insert(:post)

      assert {:ok, post} = Posts.update_post(post, @update_attrs)
      assert %Post{} = post
      assert post.body == "some updated body"
      assert post.language == "some updated language"
      assert post.title == "some updated title"
      assert post.user_id == 43
    end

    test "update_post/2 with invalid data returns error changeset" do
      post = insert(:post)
      assert %Post{} = post
      assert {:error, %Ecto.Changeset{}} = Posts.update_post(post, @invalid_attrs)
      assert post == Posts.get_post!(post.id)
    end

    test "delete_post/1 deletes the post" do
      post = insert(:post)
      assert {:ok, %Post{}} = Posts.delete_post(post)
      assert_raise Ecto.NoResultsError, fn -> Posts.get_post!(post.id) end
    end

    test "change_post/1 returns a post changeset" do
      post = insert(:post)
      assert %Ecto.Changeset{} = Posts.change_post(post)
    end

    test "publish_post/1 sets published_at on unpublished post" do
      {:ok, post} =
        insert(:post)
        |> Posts.publish_post()

      assert %DateTime{} = post.published_at
      diff = DateTime.diff(Timex.now(), post.published_at, :milliseconds)
      assert diff <= 1000
    end

    test "publish_post/1 returns error tuple for published post" do
      post = published_post()
      assert {:error, _} = Posts.publish_post(post)
    end

    test "unpublish_post/1 erases publish_at on published post" do
      {:ok, post} = published_post() |> Posts.unpublish_post()
      assert is_nil(post.published_at)
    end

    test "unpublish_post/1 returns error tuple for unpublished post" do
      post = insert(:post)
      assert {:error, _} = Posts.unpublish_post(post)
    end
  end
end
