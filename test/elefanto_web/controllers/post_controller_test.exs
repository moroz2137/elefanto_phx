defmodule ElefantoWeb.PostControllerTest do
  use ElefantoWeb.ConnCase

  import Elefanto.Factory

  describe "index" do
    setup [:create_published_posts]

    test "lists all posts", %{conn: conn, posts: posts} do
      conn = get(conn, post_path(conn, :index))
      assert conn.assigns.posts -- posts == []
      assert posts -- conn.assigns.posts == []
      assert html_response(conn, 200)
    end
  end

  describe "new post" do
    test "redirects to /blog when not signed in", %{conn: conn} do
      conn = get(conn, post_path(conn, :new))
      assert redirected_to(conn) == "/blog"
    end

    test "renders html response when signed in", %{conn: conn} do
      conn = get(conn_with_session(), post_path(conn, :new))

      assert html_response(conn, 200)
    end
  end

  describe "edit post" do
    setup [:create_post]

    test "redirects to /blog when not signed in", %{conn: conn, post: post} do
      conn = get(conn, post_path(conn, :edit, post))
      assert redirected_to(conn) == "/blog"
    end

    test "renders html response when signed in", %{conn: conn, post: post} do
      conn = get(conn_with_session(), post_path(conn, :edit, post))

      assert html_response(conn, 200)
    end
  end

  describe "delete post" do
    setup [:create_post]

    test "deletes chosen post", %{conn: conn, post: post} do
      conn = delete(conn_with_session(), post_path(conn, :delete, post))

      assert redirected_to(conn) == post_path(conn, :index)

      conn = get(build_conn(), post_path(conn, :show, post))
      assert response(conn, 404)
    end
  end

  defp conn_with_session do
    user = insert(:user)

    build_conn()
    |> Plug.Test.init_test_session(user_id: user.id)
  end

  defp create_published_posts(_) do
    # posts in posts#index are ordered
    # in a descending order by number
    posts =
      Enum.map(1..2, fn _ -> published_post() end)
      |> Enum.reverse()

    {:ok, posts: posts}
  end

  defp create_post(_) do
    post = insert(:post)
    {:ok, post: post}
  end
end
