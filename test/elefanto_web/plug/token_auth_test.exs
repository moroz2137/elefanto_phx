defmodule ElefantoWeb.TokenAuthTest do
  use ElefantoWeb.ConnCase, async: true

  alias ElefantoWeb.TokenAuth
  import Elefanto.Factory

  describe "call/2" do
    test "halts conn with 401 when no Authorization header is present", %{conn: conn} do
      conn = TokenAuth.call(conn, [])
      assert conn.halted
      assert conn.status == 401
    end

    test "halts conn with 401 when Authorization header is not a Bearer token", %{conn: conn} do
      conn =
        conn
        |> put_req_header("authorization", "foobar")
        |> TokenAuth.call([])

      assert conn.halted
      assert conn.status == 401
    end

    defp put_bearer_token_for(conn, user) do
      token = TokenAuth.issue_for(user)
      put_req_header(conn, "authorization", "Bearer #{token}")
    end

    test "does not halt conn with valid user_id present", %{conn: conn} do
      conn =
        conn
        |> put_bearer_token_for(insert(:user))
        |> TokenAuth.call([])

      refute conn.halted
      refute conn.status == 401
    end

    test "assigns current_user when valid Bearer token is present", %{conn: conn} do
      user = insert(:user)

      conn =
        conn
        |> put_bearer_token_for(user)
        |> TokenAuth.call([])

      assert %{assigns: %{current_user: %Elefanto.Accounts.User{} = assigned_user}} = conn
      assert assigned_user.id == user.id
    end
  end
end
