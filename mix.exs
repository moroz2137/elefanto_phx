defmodule Elefanto.Mixfile do
  use Mix.Project

  def project do
    [
      app: :elefanto,
      version: "0.1.2",
      elixir: "~> 1.4",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Elefanto.Application, []},
      extra_applications: [:logger, :runtime_tools, :pandex]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      # Base
      {:phoenix, "~> 1.3.2"},
      {:phoenix_pubsub, "~> 1.0"},
      {:phoenix_ecto, "~> 3.2"},
      {:postgrex, ">= 0.0.0"},
      {:phoenix_html, "~> 2.10"},
      {:phoenix_live_reload, "~> 1.0", only: :dev},
      {:gettext, "~> 0.11"},
      {:cowboy, "~> 1.0"},
      {:ecto_network, "~> 0.6.0"},
      {:browser, "~> 0.1.0"},

      # API
      {:ja_serializer, "~> 0.13"},

      # Security
      {:comeonin, "~> 4.0"},
      {:argon2_elixir, "~> 1.2"},
      {:recaptcha, "~> 2.3"},

      # Views
      {:pandex, "~> 0.1"},
      {:earmark, "~> 1.2"},
      {:scrivener_ecto, "~> 1.3.0"},
      {:scrivener_html, "~> 1.7"},
      {:timex, "~> 3.1"},
      {:phoenix_html_sanitizer, "~> 1.0.0"},
      {:slugify, "~> 1.1"},

      # Deployment
      {:edeliver, "~> 1.5.0"},
      {:distillery, "~> 1.5.3"},
      {:rollbax, ">= 0.0.0"},

      # Testing
      {:ex_machina, "~> 2.2", only: :test},

      # Caching
      {:con_cache, "~> 0.13.0"},

      # AWS S3
      {:ex_aws, "~> 2.0"},
      {:ex_aws_s3, "~> 2.0"},
      {:hackney, "~> 1.9"},
      {:sweet_xml, "~> 0.6"},
      {:arc, "~> 0.10.0"},
      {:httpoison, "~> 0.13"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test --trace"]
    ]
  end
end
