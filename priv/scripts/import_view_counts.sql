create temp table post_view_counts (
  id serial primary key,
  view_count integer
);

copy post_view_counts (id, view_count) from
'/Users/karol/Desktop/post-views.csv' delimiter ',' csv header;

update posts p1 set view_count = p2.view_count from post_view_counts p2 where p1.id = p2.id;
