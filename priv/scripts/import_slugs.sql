create temp table post_slugs (
  id serial primary key,
  slug varchar not null
);

copy post_slugs (id, slug)
from '/Users/karol/Desktop/post-slugs.csv'
delimiter ',' csv header;

update posts p1 set slug = p2.slug
from post_slugs p2 where p1.id = p2.id;

drop table if exists post_slugs;