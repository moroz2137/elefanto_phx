create temp table post_published_at (
  id serial primary key,
  published_at timestamp not null
);

copy post_published_at (id, published_at) from
'/Users/karol/Desktop/post-published-at.csv'
delimiter ',' csv header;

update posts p1 set published_at = p2.published_at
from post_published_at p2 where p1.id = p2.id;

update posts p1 set published_at = p1.inserted_at where published_at is null;