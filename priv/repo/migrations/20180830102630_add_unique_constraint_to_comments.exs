defmodule Elefanto.Repo.Migrations.AddUniqueConstraintToComments do
  use Ecto.Migration

  def up do
    execute(
      "DELETE FROM comments c1 WHERE EXISTS (SELECT 1 FROM comments c2 WHERE c1.body = c2.body AND c1.id > c2.id)"
    )

    create(unique_index(:comments, [:body, :post_id]))
  end

  def down do
    drop(index(:comments, [:body, :post_id]))
  end
end
