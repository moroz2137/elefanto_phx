defmodule Elefanto.Repo.Migrations.AddPublishedAtToPosts do
  use Ecto.Migration

  def change do
    alter table(:posts) do
      add(:published_at, :timestamp)
    end
  end
end
