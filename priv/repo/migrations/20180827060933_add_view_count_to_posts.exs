defmodule Elefanto.Repo.Migrations.AddViewCountToPosts do
  use Ecto.Migration

  def change do
    alter table(:posts) do
      add(:view_count, :integer, null: false, default: 0)
    end
  end
end
