defmodule Elefanto.Repo.Migrations.AddRenderingEngineToPosts do
  use Ecto.Migration

  def up do
    alter table("posts") do
      add(:rendering_engine, :string)
    end

    execute("update posts set rendering_engine = 'legacy' where not textile_enabled")
    execute("update posts set rendering_engine = 'textile' where textile_enabled")

    alter table("posts") do
      remove(:textile_enabled)
    end
  end

  def down do
    alter table("posts") do
      add(:textile_enabled, :boolean)
    end

    execute("update posts set textile_enabled = (rendering_engine = 'textile')")

    alter table("posts") do
      remove(:rendering_engine)
    end
  end
end
