defmodule Elefanto.Repo.Migrations.CreateFiles do
  use Ecto.Migration

  def change do
    create table(:files) do
      add(:filename, :string)
      add(:slug, :string)
      add(:description, :text)

      timestamps()
    end
  end
end
