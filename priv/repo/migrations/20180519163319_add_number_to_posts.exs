defmodule Elefanto.Repo.Migrations.AddNumberToPosts do
  use Ecto.Migration

  def change do
    alter table(:posts) do
      add(:number, :float)
    end
  end
end
