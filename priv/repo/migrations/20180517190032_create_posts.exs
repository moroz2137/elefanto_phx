defmodule Elefanto.Repo.Migrations.CreatePosts do
  use Ecto.Migration

  def change do
    create table(:posts) do
      add :title, :string
      add :user_id, :integer
      add :body, :text
      add :language, :string

      timestamps()
    end

  end
end
