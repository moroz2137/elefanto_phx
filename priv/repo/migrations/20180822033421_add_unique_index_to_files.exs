defmodule Elefanto.Repo.Migrations.AddUniqueIndexToFiles do
  use Ecto.Migration

  def change do
    create(unique_index(:files, [:slug]))
  end
end
