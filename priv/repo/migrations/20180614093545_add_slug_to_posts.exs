defmodule Elefanto.Repo.Migrations.AddSlugToPosts do
  use Ecto.Migration

  def change do
    alter table(:posts) do
      add(:slug, :string, null: false, default: fragment("SUBSTR(MD5(RANDOM()::TEXT), 0, 20)"))
    end

    create(index(:posts, :slug, unique: true))
  end
end
