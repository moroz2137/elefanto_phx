defmodule Elefanto.Repo.Migrations.AddTextileEnabledToPosts do
  use Ecto.Migration

  def change do
    alter table(:posts) do
      add(:textile_enabled, :boolean, null: false)
    end
  end
end
