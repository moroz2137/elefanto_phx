use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :elefanto, ElefantoWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

{username, 0} = System.cmd("whoami", [])
username = String.trim(username)

# Configure your database
config :elefanto, Elefanto.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: username,
  password: "7R2h2djZ",
  database: "elefanto_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

config :con_cache, :config,
  ttl_check_interval: 0,
  global_ttl: 0,
  touch_on_read: false,
  name: :post_cache

config :argon2_elixir,
  t_cost: 1,
  m_cost: 8

config :recaptcha, public_key: "foobar", secret_key: "foobar_secret"

config :elefanto, enable_recaptcha: false
config :elefanto, token_salt: "foobar"
