# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :elefanto, ecto_repos: [Elefanto.Repo]

# Configures the endpoint
config :elefanto, ElefantoWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "jB90VkCEWtm/4viL2h+cvW3LOpTDY3diemRxpzYx7pigR9VROnXFDt85bqNB7MB3",
  render_errors: [view: ElefantoWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Elefanto.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

config :phoenix, :format_encoders, "json-api": Poison

config :plug, :mimes, %{
  "application/vnd.api+json" => ["json-api"]
}

config :elefanto, ElefantoWeb.Gettext, default_locale: "en", locales: ~w(en zh)

config :scrivener_html, routes_helper: ElefantoWeb.Router.Helpers, view_style: :materialize

config :rollbax, enabled: false

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
